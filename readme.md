# indigo-api

A simple API allowing you to use Indigo's medical diagnosis models from anywhere!


## Run Locally

```bash
git clone https://codeberg.org/alice/indigo-api
```

```bash
cd indigo-api/
```

```
pip install -r requirements.txt
```

```
cd indigo_api/
```

```
export FLASK_APP=web
```

```
flask run
```


## Usage

The API provides two simple routes: `/prior` and `/posterior`. The former is used to get a prior distribution over the possible medical conditions a patient may have. This is returned in the following JSON data structure:


```js
{
    medicalConditionNames: Array,
    medicalConditionPrior: Array 
}
```

* `medicalConditionNames` contains identifiers for the set of medical conditions under consideration.
* `medicalConditionPrior` contains the prior probabilities assigned to each condition. The `i`th value in `medicalConditionPrior` contains the prior probability of the `i`th condition in `medicalConditionNames`.


The second route, `/posterior` is used to get an updated distribution based on the presence or absance of patient symptoms. You're required to pass the following structure in the body of requests made to this route:

```js
{
    priorDistribution: Array,
    symptomsMap: {
        [symptomSentence]: Boolean
	...
    }
}
```

* `priorDistribution` is a prior probability distribution over the medical conditions. For example, this could be the distribution returned by `/prior`, or a previous posterior distribution returned by `/posterior`. The number of elements in `priorDistribution` must always remain the same as that in `/prior`.

* `symptomsMap` is an object specifying a list of symptoms and whether they're present or absent. For example:

   ```js
   {
       'headache': False,
       'feeling tired': True,
       'having a high temperature': True
   }
   ```

   Note that these symptoms are allowed to be natural language. The underlying language model converts these two fine-tuned embeddings in latent space, so paraphrasing is tolorable. Shorter descriptions are preferable but descriptions of up to three sentences can be tolorated.

On a succesfull request, the `/posterior` route returns the following structure:

```js
{
    posteriorDistribution: Array,
    posteriorMode: [
        medicalConditionName,
        medicalConditionSymptoms
    ]
}
```

* `posteriorDistribution` is the posterior distribution, given the symptom mapping.
* `posteriorMode` is a tuple containing the identifier of the most likely condition a posteriori, and the symptoms for this condition. Note that since our dataset only contains *positive* symptoms, `medicalConditionSymptoms` only tells you which symptoms are typically *present* when experiencing this condition, but not which are typically absent.
