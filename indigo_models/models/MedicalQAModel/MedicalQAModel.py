from sklearn.tree._tree import TREE_LEAF


class MedicalQAModel:

    def __init__(self, tree, feature_names):
        self.tree = tree
        self.root = tree
        self.symptomMap = {}
        self.feature_names = feature_names


    def getRootFeatureName(self):
        return self.feature_names[ self.root.feature ]


    def reset(self):
        self.root = self.tree
        self.symptomMap = {}


    def descend(self, yes=True):

        child = self.root.right if yes else self.root.left
        featureName = self.getRootFeatureName()

        self.symptomMap[featureName] = yes

        if child == None or child == TREE_LEAF:
            return self.symptomMap

        self.root = child
