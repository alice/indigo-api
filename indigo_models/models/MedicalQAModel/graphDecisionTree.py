# create a graph from an sklearn decision tree.

from sklearn.tree._tree import TREE_LEAF
from . Node import Node


def graphDecisionTree(tree, id=0):

    # initialise the node

    node = Node()
    node.id = id
    node.value = tree.value[id]
    node.feature = tree.feature[id]
    node.threshold = tree.threshold[id]

    # recurse to the left and right subtrees if present

    left_subtree = tree.children_left[id]
    right_subtree = tree.children_right[id]

    node.left = left_subtree
    node.right = right_subtree

    if left_subtree != TREE_LEAF:
        node.left = graphDecisionTree(tree, left_subtree)
        node.right = graphDecisionTree(tree, right_subtree)

    return node
