def clampProbability(p, positive):
    p = min(p, 0.87)

    if positive:
        if p < 0.55: p = 0.5

    else:
        if p >= 0.65: p = 1 - p
        else: p = 0.5

    return p
