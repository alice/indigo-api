import torch
import numpy as np
from sentence_transformers import SentenceTransformer
from sentence_transformers import util
from . clampProbability import clampProbability


class MedicalDiagnosisModel:

    def __init__(self, conditions, transformer=None, embeddings=None, prior=None):

        self.sentenceTransformer = transformer or SentenceTransformer('all-MiniLM-L6-v2')
        self.medicalConditions = conditions
        self.medicalConditionEmbeddings = embeddings or self.computeMedicalConditionEmbeddings()
        self.medicalConditionPrior = prior or self.computePriorDistribution(len(conditions))



    def computeMedicalConditionEmbeddings(self):

        medicalConditionEmbeddings = {}

        for medicalConditionName, medicalConditionSentences in self.medicalConditions.items():
            medicalConditionEmbeddings[medicalConditionName] = self.computeSentenceEmbeddings(self.sentenceTransformer, medicalConditionSentences)

        return medicalConditionEmbeddings



    def computeSentenceEmbeddings(self, transformer, sentences):

        embeddings = transformer.encode(sentences, convert_to_tensor=True)
        embeddings = util.normalize_embeddings(embeddings)

        return embeddings



    def computeTopCosineSimilarity(self, queryEmbeddings, corpusEmbeddings):

        semanticSearchResult = util.semantic_search(queryEmbeddings, corpusEmbeddings, score_function=util.dot_score, top_k=1)

        topCosineSimilarityIndex = 0
        topCosineSimilarityScore = semanticSearchResult[0][topCosineSimilarityIndex]['score']

        return topCosineSimilarityScore


    def computeMeanCosineSimilarity(self, queryEmbeddings, corpusEmbeddings):

        cosine_similarity = util.cos_sim(queryEmbeddings, corpusEmbeddings)
        return cosine_similarity.mean(axis=0)



    def moderateCosineSimilarity(self, cosineSimilarityScore, premiseIsPositive):
        return clampProbability(cosineSimilarityScore, premiseIsPositive)



    def computePriorDistribution(self, numberOfConclusions):
        return torch.ones(numberOfConclusions) / numberOfConclusions



    def computeLikelihoodDistribution(self, premiseSentence, conclusionEmbeddingsList, premiseIsPositive):

        likelihoodDistribution = torch.zeros(len(conclusionEmbeddingsList))
        premiseEmbeddings = self.computeSentenceEmbeddings(self.sentenceTransformer, [premiseSentence])

        for index, conclusionEmbeddings in enumerate(conclusionEmbeddingsList):

            topCosineSimilarityScore = self.computeTopCosineSimilarity(premiseEmbeddings, conclusionEmbeddings)
            likelihoodScore = self.moderateCosineSimilarity(topCosineSimilarityScore, premiseIsPositive)
            likelihoodDistribution[index] = likelihoodScore

        return likelihoodDistribution



    def computePosteriorDistribution(self, priorDistribution, likelihoodDistribution):

        print(priorDistribution.shape)
        print(likelihoodDistribution.shape)

        numeratorDistribution = priorDistribution * likelihoodDistribution
        posteriorDistribution = numeratorDistribution / numeratorDistribution.sum()

        return posteriorDistribution



    def computePosteriorDistributionFromSymptom(self, priorDistribution, symptomSentence, symptomIsPresent=True):

        medicalConditionNames = self.getMedicalConditionNames()
        medicalConditionEmbeddings = self.getMedicalConditionEmbeddings()

        likelihoodDistribution = self.computeLikelihoodDistribution(symptomSentence, medicalConditionEmbeddings, symptomIsPresent)
        posteriorDistribution = self.computePosteriorDistribution(priorDistribution, likelihoodDistribution)

        return posteriorDistribution



    def computePosteriorDistributionFromSymptoms(self, priorDistribution, symptomsMap):

        for symptomSentence, symptomIsPresent in symptomsMap.items():
            priorDistribution = self.computePosteriorDistributionFromSymptom(priorDistribution, symptomSentence, symptomIsPresent)

        return priorDistribution



    def getMedicalConditionPrior(self):
        return self.medicalConditionPrior.tolist()



    def getMedicalConditionNames(self):
        return list(self.medicalConditions)



    def getMedicalConditionNamesByIndex(self, index):

        medicalConditionNamesNp = np.array(self.getMedicalConditionNames())
        return medicalConditionNamesNp[index]



    def getMedicalConditionEmbeddings(self):
        return list(self.medicalConditionEmbeddings.values())



    def getModalMedicalCondition(self, posteriorDistribution):

        modalMedicalConditionIndex = posteriorDistribution.topk(1)
        modalMedicalConditionName = self.getMedicalConditionNamesByIndex(modalMedicalConditionIndex.indices)
        modalMedicalCondition = self.medicalConditions[modalMedicalConditionName]

        return modalMedicalConditionName, modalMedicalCondition
