from flask import Flask
from . preload import preloadModelSnapshots
from . routes import registerAppRoutes


App = Flask(__name__)


preloadModelSnapshots(App)
registerAppRoutes(App)
