import json
from torch import tensor
from flask import request
from .. functions.serialization import jsonDumps
from .. functions.serialization import jsonLoadsFromRequest
from . Route import Route


class Posterior(Route):

    def render(self):

        jsonRequest = jsonLoadsFromRequest(request)
        jsonResponse = {}

        priorDistribution = tensor(json.loads(jsonRequest['priorDistribution']))
        symptomsMap = json.loads(jsonRequest['symptomsMap'])

        posteriorDistribution = self.app.medicalDiagnosisModel.computePosteriorDistributionFromSymptoms(
            priorDistribution,
            symptomsMap
        )


        posteriorMode = self.app.medicalDiagnosisModel.getModalMedicalCondition(posteriorDistribution)


        jsonResponse['posteriorDistribution'] = posteriorDistribution.tolist()
        jsonResponse['posteriorMode'] = posteriorMode

        return jsonDumps(jsonResponse)
