class Route:

    def __init__(self, AppInstance):
        self.app = AppInstance

    def __call__(self, *args, **kwargs):
        return self.render(*args, **kwargs)

    @property
    def __name__(self):
        return self.__class__.__name__
