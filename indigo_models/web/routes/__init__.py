from flask_cors import cross_origin
from . routes import *


def registerAppRoutes(App):

    for routePath, routeRenderFunction in routesMap.items():

        cors = cross_origin()(routeRenderFunction(App))
        App.route(routePath, methods=defaultAllowedMethods)(cors)


        #pp.route(routePath, methods=defaultAllowedMethods)(routeRenderFunction(App))
