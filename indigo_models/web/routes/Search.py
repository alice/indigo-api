from flask import request
from sentence_transformers import util
from .. functions.serialization import jsonDumps
from .. functions.serialization import jsonLoadsFromRequest
from . Route import Route


class Search(Route):

    def __init__(self, app):
        super().__init__(app)

        conditionNames = list(self.app.medicalDiagnosisModel.medicalConditions.keys())
        conditionSentences = list(self.app.medicalDiagnosisModel.medicalConditions.values())
        conditionSentences = ['. '.join(conditionSentence) for conditionSentence in conditionSentences]

        conditionEmbeddings = self.app.medicalDiagnosisModel.computeSentenceEmbeddings(
            self.app.medicalDiagnosisModel.sentenceTransformer,
            conditionSentences
        )

        self.conditionNames = conditionNames
        self.conditionSentences = conditionSentences
        self.conditionEmbeddings = conditionEmbeddings


    def render(self):

        jsonRequest = jsonLoadsFromRequest(request)
        paragraph = jsonRequest['paragraph']

        sentence_embeddings = self.app.medicalDiagnosisModel.computeSentenceEmbeddings(
            self.app.medicalDiagnosisModel.sentenceTransformer,
            [paragraph],
        )

        search_result = util.semantic_search(
            sentence_embeddings,
            self.conditionEmbeddings,
            score_function=util.dot_score,
            top_k=4
        )

        best_matches = [
            {
                'name': self.conditionNames[result['corpus_id']],
                'sentence': self.conditionSentences[result['corpus_id']],
                'score': result['score']
            }

            for result in search_result[0]
        ]

        return jsonDumps(best_matches)
