import json
from flask import request
from .. functions.serialization import jsonDumps
from . Route import Route


class Question(Route):

    def render(self):

        previousAnswer = request.args.get('answer')

        if previousAnswer:
            finished = self.app.medicalQAModel.descend(
                previousAnswer == 'true'
            )

            if finished:
                json = jsonDumps(finished)
                self.app.medicalQAModel.reset()

                return json

        return jsonDumps({
            'question': self.app.medicalQAModel.getRootFeatureName()
        })
