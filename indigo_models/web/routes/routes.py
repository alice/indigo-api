from . Prior import Prior
from . Posterior import Posterior
from . Search import Search
from . Question import Question


defaultAllowedMethods = [
    'GET',
    'POST'
]


routesMap = {
    '/prior': Prior,
    '/posterior': Posterior,
    '/search': Search,
    '/question': Question
}
