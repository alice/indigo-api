from .. functions.serialization import jsonDumps
from . Route import Route


class Prior(Route):

    def render(self):

        jsonResponse = {}
        jsonResponse['medicalConditionNames'] = self.app.medicalDiagnosisModel.getMedicalConditionNames()
        jsonResponse['medicalConditionPrior'] = self.app.medicalDiagnosisModel.getMedicalConditionPrior()
        return jsonDumps(jsonResponse)

#
#
# def RoutePrior(App):
#
#     jsonPayload = {}
#     jsonPayload['medicalConditionNames'] = App.medicalDiagnosisModel.getMedicalConditionNames()
#     jsonPayload['medicalConditionPrior'] = App.medicalDiagnosisModel.getMedicalConditionPrior()
#
#     def priorRoute():
#         return json.dumps(App.medicalDiagnosisModel.medicalConditionPrior.tolist())
#
#     return priorRoute
