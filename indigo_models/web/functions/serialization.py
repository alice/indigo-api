import pickle
import json
import gzip
from flask import jsonify


def loadSnapshotFromPath(snapshotFilePath):

    with open(snapshotFilePath, 'rb') as snapshotFileReader:

        compressedSnapshot = snapshotFileReader.read()
        pickledSnapshot = gzip.decompress(compressedSnapshot)
        snapshotObject = pickle.loads(pickledSnapshot)

        return snapshotObject


def saveSnapshotToPath(snapshotFilePath, snapshotObject):

    with open(snapshotFilePath, 'wb') as snapshotFileWriter:

        pickledSnapshot = pickle.dumps(snapshotObject)
        compressedSnapshot = gzip.compress(pickledSnapshot)
        totalBytesWritten = snapshotFileWriter.write(compressedSnapshot)

        return totalBytesWritten


def jsonDumps(jsonObject): return json.dumps(jsonObject)
def jsonLoads(jsonStirng): return json.loads(jsonString)


def jsonLoadsFromRequest(flaskRequestObject):
    return jsonify(flaskRequestObject.form).json


# def loadJsonZipFromPath(jsonZipFilePath):
#
#     with open(jsonZipFilePath, 'rb') as jsonZipFileReader:
#
#         compressedJson = jsonZipFileReader.read()
#         uncompressedJson = gzip.decompress(compressedJson)
#         originalObject = json.loads(uncompressedJson)
#
#         return originalObject
