from ... models.MedicalDiagnosisModel import MedicalDiagnosisModel
from .. functions.serialization import loadSnapshotFromPath


def preloadModelSnapshots(App):
    App.medicalDiagnosisModel = loadSnapshotFromPath('./snapshots/medical-diagnosis-model-snapshot-1.pkl.gz')
    App.medicalQAModel = loadSnapshotFromPath('./snapshots/medical-qa-model-snapshot-1.pkl.gz')
